GF3UnoLuce - 24 Hour Single Hand Watch
======================================

Description
-----------
This watch face shows the time on a 24 hour scale with a single hand. Based on the Botta Uno 24 Neo.
It also shows the date, a little minute indicator and the battery level.
--------------
Many thanks to Berglauf1234, who did to whole redesign for this version. And I do like it. What do you think of it?
--------------
Please, leave your comments, bug reports and ideas on this forum:
https://forums.garmin.com/showthread.php?265723-Watchfaces-GF3-
--------------
Version 2.0:
- Removed the date/minute toggle.
- Added a small minute indicator (black triangle).
- Added a battery level indicator.
- Added support for D2 Bravo.
--------------
Version 1.1:
- Changed the date font, alignment of the previous number font was not ok.
- Changed the toggle time to 2 seconds.
- When entering low power mode, it will go back to show the date.
--------------
See:
- GF3UnoScuro for the darker version
- GF3UnoNero for the black version
