using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Lang as Lang;
using Toybox.Math as Math;
using Toybox.Time as Time;
using Toybox.Time.Gregorian as Calendar;
using Toybox.WatchUi as Ui;

class GF3UnoLuce extends Ui.WatchFace {
    var pi12 = Math.PI / 12.0;
    var pi30 = Math.PI / 30.0;
    var pi50 = Math.PI / 50.0;
    var pi6  = Math.PI / 6.0;
    var pi7  = Math.PI / 3.5;
    var cx;
    var cy;
    var width;
    var height;
    var hourHand = new [7];
    var minuteHand = new [4];
    var bckgrnd;
var orangeSmallHand = new [5];


    //! Constructor
    function initialize() {
        bckgrnd    = Ui.loadResource(Rez.Drawables.bckgrnd);
        hourHand   = [ [-3,20], [-6,3], [-6,-3], [-2,-10], [-1,-105], [1,-105], [2,-10], [6,-3], [6,3], [3,20], [0,22] ];
        minuteHand = [ [-2,-88], [-2,-109], [2,-109], [2,-88] ];
        orangeSmallHand = [[-2,0], [-2,-5], [0,-30], [2,-5], [2,0] ];
    }

    function onLayout(dc) {
        width = dc.getWidth();
        height = dc.getHeight();
        cx = width / 2;
        cy = height / 2;
        onUpdate(dc);
    }

    function onShow() {}

    function onHide() {}

    function onExitSleep() {}

    function onEnterSleep() {}

    function onUpdate(dc) {
        dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_WHITE, Gfx.COLOR_WHITE);
        dc.clear();
        dc.drawBitmap(-1, -1, bckgrnd);
        var hour= Sys.getClockTime().hour;
        var min = Sys.getClockTime().min;
        var now = Time.now();
        var info = Calendar.info(now, Time.FORMAT_SHORT);
        var batt = (Sys.getSystemStats().battery).toDouble();

        // Draw the day
        dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
        dc.drawText(cx, cy+67, Gfx.FONT_MEDIUM, info.day.format("%02d"), Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);

        // Draw the hour hand
        hour = hour + (min / 60.0);
        hour = (hour * pi12) - Math.PI;
        drawHand(dc, hour, cx, cy, hourHand, Gfx.COLOR_BLACK);

        // Draw the minute hand
        min = min * pi30;
        drawMarker(dc, min, 98, 12, Gfx.COLOR_BLACK);
                // Draw the battery hand
        drawHand(dc, batt*pi50, cx-42, cy, orangeSmallHand, Gfx.COLOR_ORANGE);

        dc.fillCircle(cx-42, cy, 4);
        // WHITE dot on the orange day / month / battery hands
        dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_TRANSPARENT);
        dc.fillCircle(cx-42, cy, 2);

        // Draw circle
        dc.setColor(Gfx.COLOR_ORANGE, Gfx.COLOR_TRANSPARENT);
        dc.fillCircle(cx, cy, 7);
        dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
        dc.fillCircle(cx, cy, 3);
    }

    function drawMarker(dc, angle, position, z, color) {
        var coords = [ [-(z/2),-position], [0, -position - z], [(z/2), -position] ];
        var result = new [coords.size()];
        var cos = Math.cos(angle);
        var sin = Math.sin(angle);

        // Transform the coordinates
        for (var i = 0; i < coords.size(); i += 1) {
            var x = (coords[i][0] * cos) - (coords[i][1] * sin) + cx;
            var y = (coords[i][0] * sin) + (coords[i][1] * cos) + cy;
            result[i] = [x, y];
        }
        dc.setColor(color, Gfx.COLOR_TRANSPARENT);
        dc.fillPolygon(result);
    }

    function drawHand(dc, angle, centerX, centerY, coords, color) {
        var result = new [coords.size()];
        var cos = Math.cos(angle);
        var sin = Math.sin(angle);

        // Transform the coordinates
        for (var i = 0; i < coords.size(); i += 1) {
            var x = (coords[i][0] * cos) - (coords[i][1] * sin) + centerX;
            var y = (coords[i][0] * sin) + (coords[i][1] * cos) + centerY;
            result[i] = [x, y];
        }
        dc.setColor(color, Gfx.COLOR_TRANSPARENT, color);
        dc.fillPolygon(result);
    }
}
