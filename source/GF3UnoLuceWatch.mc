using Toybox.Application as App;

class GF3UnoLuceWatch extends App.AppBase {
    function onStart() {}

    function onStop() {}

    function getInitialView() {
        return [new GF3UnoLuce()];
    }
}
